class AddEventDateToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :event_date, :date
  end
end
