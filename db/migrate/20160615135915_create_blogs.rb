class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.text :blog_muffin
      t.text :blog_munchkin
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :blogs, [:user_id, :created_at]
  end
end
