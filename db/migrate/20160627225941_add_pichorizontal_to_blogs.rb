class AddPichorizontalToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :pic_horizontal, :boolean, default: false
  end
end
