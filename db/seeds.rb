# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.first
15.times do
  content = Faker::Lorem.sentence(5)
  content1 = Faker::Lorem.sentence(5)
  user.blogs.create!(blog_muffin: content, blog_munchkin: content1, event_date: Date.today, pic_horizontal: false)
end

