require 'test_helper'

class BlogsEntryTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:muffin)
    @non_admin = users(:hey_you)
  end
  
  test "blog entry" do
    # Invalid entry
    get blogs_path
    log_in_as(@admin)
    assert_no_difference 'Blog.count' do
      post blogs_path, blog: { blog_muffin: "",
                               blog_munchkin: "",
                               event_date: Date.tomorrow }
    end
  end
  
end
