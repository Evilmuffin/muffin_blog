require 'test_helper'

class HomePageTestTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  
  def setup
    @user = users(:muffin)
  end
  
  test "blog display" do
    get blogs_path
    assert_template 'blogs/index'
    assert_select 'title', full_title
    Blog.paginate(page: 1, per_page: 5).each do |blog|
      assert_match blog.blog_muffin, response.body
    end
  end
end
