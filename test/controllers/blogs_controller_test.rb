require 'test_helper'

class BlogsControllerTest < ActionController::TestCase
  
  def setup
    @blog = blogs(:test1)
    @non_admin = users(:hey_you)
    @admin = users(:muffin)
  end
  
  test "should redirect create when not logged in as admin" do
    assert_no_difference 'Blog.count' do
      post :create, blog: { blog_muffin: "lkj",
                            blog_munchkin: "dkdk",
                            event_date: Date.today }
    end
    assert_match flash[:danger], 'You no Muffin!'
    assert_redirected_to root_url
    log_in_as(@non_admin)
    assert_no_difference 'Blog.count' do
      post :create, blog: { blog_muffin: "lkj",
                            blog_munchkin: "dkdk",
                            event_date: Date.today }
    end
    assert_match flash[:danger], 'You no Muffin!'
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in as admin" do
    assert_no_difference 'Blog.count' do
      delete :destroy, id: @blog
    end
    assert_match flash[:danger], 'You no Muffin!'
    assert_redirected_to root_url
    log_in_as(@non_admin)
    assert_no_difference 'Blog.count' do
      delete :destroy, id: @blog
    end
    assert_match flash[:danger], 'You no Muffin!'
    assert_redirected_to root_url
  end
  
  
  
end
