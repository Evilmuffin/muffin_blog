require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  def setup
    @title = "Evilmuffin and the Munchkin from Hell"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | #{@title}"
  end
  
  test "should get muffin" do
    get :muffin
    assert_response :success
  end
  
  test "should get munchkin" do
    get :munchkin
    assert_response :success
  end

  test "Only admin should get welcome page" do
    get :welcome
    assert_match flash[:danger], "You no Muffin!"
    assert_redirected_to root_path
  end

end
