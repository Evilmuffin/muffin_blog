require 'test_helper'

class BlogTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:muffin)
    @blog = @user.blogs.build(blog_muffin: "Testing",
                              blog_munchkin: "Testing2",
                              event_date: "01/02/2016")
  end
  
  test "should be valid" do
    assert @blog.valid?
  end
  
  test "user id should be present" do
    @blog.user_id = nil
    assert_not @blog.valid?
  end
  
  test "blog_muffin should be present" do
    @blog.blog_muffin = ""
    assert_not @blog.valid?
  end
  
  test "event_date should be present" do
    @blog.event_date = ""
    assert_not @blog.valid?
  end
  
  test "event_date should be in correct format" do
    # date format "dd/mm/yyyy"
    @blog.event_date = "133/133/111"
    assert_not @blog.valid?
    @blog.event_date = "1/30/16"
    assert_not @blog.valid?
    @blog.event_date = "01/30/2016"
    assert_not @blog.valid?
    @blog.event_date = "11/12/1900"
    assert @blog.valid?
  end
  
  test "event_date should not be future date" do
    @blog.event_date = Date.tomorrow
    assert_not @blog.valid?
  end
  
  test "blog_munchkin should be allowed blank" do
    @blog.blog_munchkin = ""
    assert @blog.valid?
  end
  
  test "blog_muffin should has 200 characters max" do
    @blog.blog_muffin = "a" * 1001
    assert_not @blog.valid?
  end
  
  test "blog_munchkin should has 200 characters max" do
    @blog.blog_munchkin = "a" * 1001
    assert_not @blog.valid?
  end
  
end
