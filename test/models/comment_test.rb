require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:muffin)
    @blog = blogs(:test1)
    @comment = @blog.comments.build(content: "Testing")
    @comment.user_id = @user.id
  end
  
  test "should be valid" do
    assert @comment.valid?
  end
  
  test "blog id should be present" do
    @comment.blog_id = nil
    assert_not @comment.valid?
  end
  
  test "user id should be present" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end
  
  test "content should be present" do
    @comment.content = " "
    assert_not @comment.valid?
  end
  
  test "comment length should be less than 150 chars" do
    @comment.content = "a" * 151
    assert_not @comment.valid?
  end
  
end
