A pretty simple blog page written in Ruby on Rails.  

Login is done with omniauth-facebook.  
Admin privilege can only be granted using console.  
Images are uploaded to AWS.

It has a comments model as well, but was replaced by
facebook comments plugin instead.