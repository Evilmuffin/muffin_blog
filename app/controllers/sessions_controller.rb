class SessionsController < ApplicationController
  
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    log_in(user)
    if user.admin?
      flash[:success] = "Hi Muffin!"
    else
      flash[:success] = "Hi #{user.name}!"
    end
    if user.admin?
      redirect_back_or(welcome_path)
    else
      redirect_back_or(root_path)
    end
  end
  
  def destroy
    flash[:success] = "Bye!"
    session[:user_id] = nil
    redirect_to root_path
  end
  
end
