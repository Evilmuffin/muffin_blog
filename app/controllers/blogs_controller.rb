class BlogsController < ApplicationController
  before_action :admin_user, only: [:new, :create, :destroy]
  
  def home
    @blogs = Blog.all
    @random_blogs = Blog.where.not(picture: nil).sample(3)
    @recent_blogs = Blog.where.not(picture: nil).first(3)
  end
  
  def index
    @blogs = Blog.paginate(page: params[:page], per_page: 5)
  end
  
  def new
    @blog = current_user.blogs.build
  end
  
  def create
    @blog = current_user.blogs.build(blog_params)
    if @blog.save
      flash[:success] = "Entry Saved"
      redirect_to blogs_path
    else
      render 'new'
    end
  end
  
  def show
    @blog = Blog.find(params[:id])
    @comment = Comment.new
  end
  
  def edit
    @blog = Blog.find(params[:id])
  end
  
  def update
    @blog = Blog.find(params[:id])
    if @blog.update_attributes(blog_params)
      flash[:success] = "Entry updated"
      redirect_to blog_path(@blog)
    else
      render 'edit'
    end
  end
  
  def destroy
    Blog.find(params[:id]).destroy
    flash[:success] = "Entry deleted"
    redirect_to blogs_path
  end
  
  private
  
    def blog_params
      params.require(:blog).permit(:blog_muffin, :blog_munchkin, 
                                   :event_date, :picture, :pic_horizontal,
                                   :remove_picture)
    end
  
end
