class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  include SessionsHelper
  
  private
  
    def logged_in_user
      unless current_user
        store_location
        flash[:danger] = "Please log in!"
        redirect_to(root_url)
      end
    end
    
    def admin_user
      if current_user
        unless current_user.admin?
          store_location
          flash[:danger] = "You no Muffin!"
          redirect_to(root_url)
        end
      else
        store_location
        flash[:danger] = "You no Muffin!"
        redirect_to(root_url)
      end
    end
    
    def correct_user
      @blog = Blog.find(params[:blog_id])
      @comment = @blog.comments.find(params[:id])
      @user = @comment.user
      redirect_to(root_url) unless current_user?(@user) || current_user.admin?
    end
    
end
