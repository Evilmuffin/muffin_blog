class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :update, :edit, :destroy]
  before_action :correct_user, only: [:update, :edit, :destroy]
  
  def create
    @blog = Blog.find(params[:blog_id])
    @comment = @blog.comments.create(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash[:success] = "Comment saved"
      redirect_to blog_path(@blog)
    else
      flash[:danger] = "Comment was either blank or more than 150 characters."
      redirect_to blog_path(@blog)
    end
  end
  
  def edit
    @blog = Blog.find(params[:blog_id])
    @comment = @blog.comments.find(params[:id])
  end
  
  def update
    @blog = Blog.find(params[:blog_id])
    @comment = @blog.comments.find(params[:id])
    if @comment.update_attributes(comment_params)
      flash[:success] = "Comment updated"
      redirect_to blog_path(@blog)
    else
      render 'edit'
    end
  end
  
  def destroy
    @blog = Blog.find(params[:blog_id])
    @comment = @blog.comments.find(params[:id])
    @comment.destroy
    flash[:success] = "Comment deleted."
    redirect_to blog_path(@blog)
  end
  
  private
    def comment_params
      params.require(:comment).permit(:content)
    end
      
  
end
