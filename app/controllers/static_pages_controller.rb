class StaticPagesController < ApplicationController
  before_action :admin_user, only: [:welcome]
  
  def muffin
  end
  
  def munchkin
  end
  
  def welcome
  end

  def about
  end
end
