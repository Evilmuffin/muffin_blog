class PicturesController < ApplicationController
  
  def index
    @blogs = Blog.where.not(picture: nil).paginate(page: params[:page], per_page: 9)
  end
  
  def show
    @blog = Blog.find(params[:id])

    if @blog == Blog.where.not(picture: nil).last
      @older_blog = @blog   # Place holder code for now
    else
      @older_blog = @blog.older
    end
    
    if @blog == Blog.where.not(picture: nil).first
      @newer_blog = @blog   # Place holder code for now
    else
      @newer_blog = @blog.newer
    end
  end
  
end
