module SessionsHelper
  
  def log_in(user)
    session[:user_id] = user.id
  end
  
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
  
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  def current_user?(user)
    user == current_user
  end
  
end
