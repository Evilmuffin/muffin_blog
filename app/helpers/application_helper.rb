module ApplicationHelper
  
  def full_title(title = '')
    base_title = "Evilmuffin and the Munchkin from Hell"
    if title.empty?
      base_title
    else
      title + " | " + base_title
    end
  end
  
  
end
