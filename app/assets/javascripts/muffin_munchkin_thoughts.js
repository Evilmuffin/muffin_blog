$(document).ready(function(){
  $(".clicky_blog").click(function(){
    var blog = $(this).data('blogid');
    var heading = $(this).data('headingid');
    var thought = $(this).data('thoughtid');
    if ($(this).attr('data-click-state') == "muffin") {
      document.getElementById(blog).innerHTML = $(this).data('munchkin');
      document.getElementById(thought).innerHTML = "Go to Muffin's Thought";
      document.getElementById(heading).innerHTML = "Munchkin's World";
      $(this).attr('data-click-state', 'munchkin');
    }
    else {
      document.getElementById(blog).innerHTML = $(this).data('muffin');
      document.getElementById(thought).innerHTML = "Go to Munchkin's World";
      document.getElementById(heading).innerHTML = "Muffin's Thought";
      $(this).attr('data-click-state', 'muffin');
    }
  });
});