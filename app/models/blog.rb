class Blog < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy
  
  default_scope -> { order(event_date: :desc, created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates(:user_id, presence: true)
  validate(:check_muffin)
  validate(:check_munchkin)
  validates(:event_date, presence: true)
  validate(:is_future_today)
  validate(:picture_size)
  before_create :pic_orientation
  
  def same_day_blog
    Blog.where.not(picture: nil).where("event_date = ?", self.event_date)
  end
  
  def older
    if self.id == same_day_blog.last.id
      # If the record is on the last record on a given event_date,
      # the older record will be the first on the previous event_date.
      Blog.where.not(picture: nil).where("event_date < ?", 
                                          self.event_date).first
    else
      # If not, the next record will be the first on the previous created_at.
      same_day_blog.where("created_at < ?", self.created_at).first
    end
  end
  
  def newer   # The reverse of "older"
    if self.id == same_day_blog.first.id
      Blog.where.not(picture: nil).where("event_date > ?", 
                                           self.event_date).last
    else
      same_day_blog.where("created_at > ?", self.created_at).last
    end
  end
  
  private
  
    def pic_orientation
      if self.picture?
        img = MiniMagick::Image::read(picture)
        if img[:width] > img[:height]
          self.pic_horizontal = true
        end
      end
    end
  
    def check_muffin
      if blog_muffin.blank?
        errors.add(:base, "Muffin's thought is blank")
      end
      if blog_muffin.length > 1000
        errors.add(:base, "Muffin's thought is too long (Max 200 characters)")
      end
    end
    
    def check_munchkin
      if blog_munchkin.length > 1000
        errors.add(:base, "Munchkin's thought is too long (Max 200 characters)")
      end
    end
    
    def is_future_today
      if event_date
        if event_date > Date.today
          errors.add(:base, "Date should not be a future date")
        end
      end
    end
    
    def picture_size
      if picture.size > 4.megabytes
        errors.add(:base, "Upload size limited to 4MB")
      end
    end
    
end
